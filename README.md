# PokaDek

A Pokémon lookup utility for android using the [pokeapi](https://pokeapi.co)

## Download

Get the latest release [here](https://gitlab.com/Doomsdayrs/pokadek/-/releases/permalink/latest)

## How to use

There are two ways to use PokaDek

1. Select text, and click the action "PokaDek"
2. Select text, and share it to the app called "PokaDek"
