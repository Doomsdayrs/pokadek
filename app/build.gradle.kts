plugins {
	id("com.android.application")
	id("org.jetbrains.kotlin.android")
	kotlin("plugin.serialization") version "1.6.21"
}

android {
	compileSdk = 32

	defaultConfig {
		applicationId = "com.github.doomsdayrs.apps.pokadek"
		minSdk = 23
		targetSdk = 32
		versionCode = 2
		versionName = "0.1.1"

		testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
		vectorDrawables {
			useSupportLibrary = true
		}
	}

	buildTypes {
		release {
			isMinifyEnabled = false
			proguardFiles(
				getDefaultProguardFile("proguard-android-optimize.txt"),
				"proguard-rules.pro"
			)
		}
	}
	compileOptions {
		sourceCompatibility = JavaVersion.VERSION_11
		targetCompatibility = JavaVersion.VERSION_11
	}
	kotlinOptions {
		jvmTarget = "11"
	}
	buildFeatures {
		compose = true
	}
	composeOptions {
		kotlinCompilerExtensionVersion = rootProject.extra["compose_version"] as String
	}
	packagingOptions {
		resources {
			excludes += "/META-INF/{AL2.0,LGPL2.1}"
		}
	}
	namespace = "com.github.doomsdayrs.apps.pokadek"
}

dependencies {

	implementation("androidx.core:core-ktx:1.8.0")
	implementation("com.google.android.material:material:1.6.1")

	// Serialization
	implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.3")

	// OkHTTP
	implementation("com.squareup.okhttp3:okhttp-android:5.0.0-alpha.7")
	implementation("com.squareup.okhttp3:okhttp-coroutines:5.0.0-alpha.7")

	// Cache
	implementation("com.google.guava:guava:31.1-android")

	// Compose
	implementation("androidx.activity:activity-compose:1.4.0")
	implementation("io.coil-kt:coil-compose:2.1.0")
	implementation("androidx.compose.ui:ui:${rootProject.extra["compose_version"]}")
	implementation("androidx.compose.foundation:foundation:${rootProject.extra["compose_version"]}")
	implementation("androidx.compose.material3:material3:1.0.0-alpha13")
	implementation("androidx.compose.ui:ui-tooling-preview:${rootProject.extra["compose_version"]}")
	implementation( "com.google.accompanist:accompanist-pager:0.24.10-beta")

	// KTX
	implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.4.1")
	implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.4.1")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.6.2")



	testImplementation("junit:junit:4.13.2")
	androidTestImplementation("androidx.test.ext:junit:1.1.3")
	androidTestImplementation("androidx.test.espresso:espresso-core:3.4.0")
	androidTestImplementation("androidx.compose.ui:ui-test-junit4:${rootProject.extra["compose_version"]}")
	debugImplementation("androidx.compose.ui:ui-tooling:${rootProject.extra["compose_version"]}")
	debugImplementation("androidx.compose.ui:ui-test-manifest:${rootProject.extra["compose_version"]}")
}