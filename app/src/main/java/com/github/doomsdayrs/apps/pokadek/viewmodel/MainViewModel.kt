package com.github.doomsdayrs.apps.pokadek.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.doomsdayrs.apps.pokadek.domain.model.PokemonInfo
import com.github.doomsdayrs.apps.pokadek.domain.repository.IPokemonRepository
import com.github.doomsdayrs.apps.pokadek.domain.repository.PokemonRepository
import kotlinx.coroutines.flow.*

class MainViewModel : ViewModel(), IMainViewModel {
	companion object {
		private val repo: IPokemonRepository = PokemonRepository()
	}

	private val textState: MutableStateFlow<String?> = MutableStateFlow(null)

	override val isLoading: MutableStateFlow<Boolean> = MutableStateFlow(false)

	override val exception: MutableStateFlow<Throwable?> = MutableStateFlow(null)

	override val content: Flow<PokemonInfo?> = textState.transform { name ->
		isLoading.emit(true)
		exception.emit(null)

		if (name != null) {
			emit(repo.getPokemonInfo(name.lowercase()))
		} else
			emit(null)
		isLoading.emit(false)
	}.catch {
		isLoading.emit(false)
		exception.emit(it)
		emit(null)
	}.shareIn(viewModelScope, SharingStarted.Eagerly, 2)

	override fun setText(text: CharSequence) {
		textState.tryEmit(text.toString())
	}
}