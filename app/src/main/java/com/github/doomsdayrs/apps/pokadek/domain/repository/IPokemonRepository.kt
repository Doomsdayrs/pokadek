package com.github.doomsdayrs.apps.pokadek.domain.repository

import com.github.doomsdayrs.apps.pokadek.domain.model.PokemonInfo

interface IPokemonRepository {
	suspend fun getPokemonInfo(text: String): PokemonInfo?
}