package com.github.doomsdayrs.apps.pokadek.domain.model.remote

import kotlinx.serialization.Serializable

@Serializable
data class Pokemon(
	val id: Int,
	val name: String,
	val sprites: Sprites,
	val species: Species
)

@Serializable
data class PokemonSpecies(
	val flavor_text_entries: List<FlavourTextEntry>
)

@Serializable
data class FlavourTextEntry(
	val flavor_text: String,
	val language: Language,
	val version: Version
)

@Serializable
data class Language(
	val name: String
)

@Serializable
data class Version(
	val name: String,
)

@Serializable
data class Sprites(
	val front_default: String?,
	val front_shiny: String?
)

@Serializable
data class Species(
	val name: String,
	val url: String
)