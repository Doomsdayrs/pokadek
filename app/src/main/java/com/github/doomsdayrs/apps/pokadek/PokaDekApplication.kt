package com.github.doomsdayrs.apps.pokadek

import android.app.Application
import com.google.android.material.color.DynamicColors

class PokaDekApplication : Application() {
	override fun onCreate() {
		super.onCreate()
		DynamicColors.applyToActivitiesIfAvailable(this)
	}
}