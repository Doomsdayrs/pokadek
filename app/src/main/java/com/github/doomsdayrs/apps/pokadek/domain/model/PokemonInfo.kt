package com.github.doomsdayrs.apps.pokadek.domain.model

import com.github.doomsdayrs.apps.pokadek.domain.model.remote.FlavourTextEntry
import java.net.URL

data class PokemonInfo(
	val name: String,
	val sprite: String?,
	val spriteShiny: String?,
	val description: List<FlavourTextEntry>
)
