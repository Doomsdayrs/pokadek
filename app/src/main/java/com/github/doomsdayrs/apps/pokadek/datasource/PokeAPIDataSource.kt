package com.github.doomsdayrs.apps.pokadek.datasource

import android.util.Log
import com.github.doomsdayrs.apps.pokadek.domain.model.remote.Pokemon
import com.github.doomsdayrs.apps.pokadek.domain.model.remote.PokemonSpecies
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromStream
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.executeAsync
import java.io.IOException

class PokeAPIDataSource {
	companion object {
		private const val TAG: String = "PokeAPIDataSource"
	}

	private val client = OkHttpClient().newBuilder().addInterceptor(
		Interceptor {
			Log.i(TAG, it.request().url.toString())
			it.proceed(it.request())
		}
	).build()

	private val json = Json {
		ignoreUnknownKeys = true
	}

	@OptIn(ExperimentalSerializationApi::class)
	@Throws(IOException::class)
	suspend fun getPokemon(name: String): Pokemon? {
		val pokemonRequest = Request.Builder()
			.url("https://pokeapi.co/api/v2/pokemon/$name")
			.build()

		val pokemonCall = client.newCall(pokemonRequest)

		val pokemonResponse = pokemonCall.executeAsync()

		if (!pokemonResponse.isSuccessful)
			return null;

		return try {
			json.decodeFromStream(pokemonResponse.body.byteStream())
		} catch (e: SerializationException) {
			e.printStackTrace()
			null
		}
	}

	@OptIn(ExperimentalSerializationApi::class)
	@Throws(IOException::class)
	suspend fun getSpecies(id: Int): PokemonSpecies? {
		val speciesRequest = Request.Builder()
			.url("https://pokeapi.co/api/v2/pokemon-species/$id")
			.build()

		val speciesCall = client.newCall(speciesRequest)

		val speciesResponse = speciesCall.executeAsync()

		if (!speciesResponse.isSuccessful)
			return null;

		return try {
			json.decodeFromStream(speciesResponse.body.byteStream())
		} catch (e: SerializationException) {
			e.printStackTrace()
			null
		}
	}

}