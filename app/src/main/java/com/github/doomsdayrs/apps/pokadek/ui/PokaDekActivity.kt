package com.github.doomsdayrs.apps.pokadek.ui

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.github.doomsdayrs.apps.pokadek.R
import com.github.doomsdayrs.apps.pokadek.domain.model.PokemonInfo
import com.github.doomsdayrs.apps.pokadek.ui.theme.PokaDekTheme
import com.github.doomsdayrs.apps.pokadek.viewmodel.IMainViewModel
import com.github.doomsdayrs.apps.pokadek.viewmodel.MainViewModel
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState
import kotlinx.coroutines.launch


class PokaDekActivity : ComponentActivity() {
	private val viewModel: IMainViewModel by viewModels<MainViewModel>()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		val text = intent.getCharSequenceExtra(Intent.EXTRA_PROCESS_TEXT) ?: intent.getStringExtra(
			Intent.EXTRA_TEXT
		) ?: return
		viewModel.setText(text)

		setContent {
			val isLoading by viewModel.isLoading.collectAsState(true)
			val animal by viewModel.content.collectAsState(null)
			val exception by viewModel.exception.collectAsState(null)

			PokaDekTheme {
				MainContent(isLoading, animal, exception)
			}
		}
	}
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalPagerApi::class)
@Composable
fun MainContent(
	isLoading: Boolean,
	animal: PokemonInfo?,
	exception: Throwable?
) {
	Card(
		modifier = Modifier
			.fillMaxWidth(),
	) {
		Box(
			modifier = Modifier.fillMaxWidth()
		) {
			if (isLoading)
				CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
			else {
				if (exception != null) {
					Text(
						exception.stackTraceToString(),
						modifier = Modifier
							.align(Alignment.Center)
							.verticalScroll(rememberScrollState())
					)
				} else
					if (animal != null) {
						Column(
							horizontalAlignment = Alignment.CenterHorizontally,
							modifier = Modifier.padding(16.dp)
						) {
							Text(
								stringResource(R.string.app_name),
								style = MaterialTheme.typography.titleLarge,
								fontWeight = FontWeight.Bold
							)
							AsyncImage(
								ImageRequest.Builder(LocalContext.current)
									.data(animal.sprite)
									.crossfade(true)
									.build(),
								animal.name,
								modifier = Modifier
									.size(128.dp)
							)

							Text(animal.name, style = MaterialTheme.typography.titleLarge)

							Divider()
							val pagerState = rememberPagerState()

							HorizontalPager(
								count = animal.description.size,
								modifier = Modifier
									.fillMaxWidth()
									.height(150.dp),
								state = pagerState
							) {
								val desc = animal.description[it]
								Column {
									Text(
										desc.version.name,
										style = MaterialTheme.typography.titleMedium,
										modifier = Modifier.fillMaxWidth(),
										textAlign = TextAlign.Center
									)

									Divider()

									Text(
										desc.flavor_text,
										style = MaterialTheme.typography.bodyMedium,
										modifier = Modifier
											.fillMaxWidth()
											.height(100.dp)
											.verticalScroll(rememberScrollState())
									)
								}
							}

							val scope = rememberCoroutineScope()
							if (animal.description.size > 1) {
								Row {
									IconButton(
										onClick = {
											scope.launch {
												val newPage = pagerState.currentPage - 1
												if (newPage >= 0)
													pagerState.scrollToPage(newPage)
											}
										}
									) {
										Icon(
											Icons.Default.ArrowBack, null,
										)
									}
									IconButton(
										onClick = {
											scope.launch {
												val newPage = pagerState.currentPage + 1
												if (newPage < pagerState.pageCount)
													pagerState.scrollToPage(newPage)
											}
										}
									) {
										Icon(
											Icons.Default.ArrowForward, null,
										)
									}
								}
							}
						}
					} else {
						Text(
							"No such Pokémon!",
							modifier = Modifier
								.align(Alignment.Center)
						)
					}
			}
		}

	}
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
	PokaDekTheme {
		MainContent(false, null, null)
	}
}