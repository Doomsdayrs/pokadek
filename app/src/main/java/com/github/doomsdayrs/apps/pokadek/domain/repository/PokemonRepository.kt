package com.github.doomsdayrs.apps.pokadek.domain.repository

import android.util.Log
import com.github.doomsdayrs.apps.pokadek.datasource.PokeAPIDataSource
import com.github.doomsdayrs.apps.pokadek.domain.model.PokemonInfo
import com.google.common.cache.Cache
import com.google.common.cache.CacheBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit

class PokemonRepository : IPokemonRepository {
	companion object {
		private const val TAG: String = "PokemonRepository"
	}

	private val datasource = PokeAPIDataSource()
	private val cache: Cache<String, PokemonInfo?> = CacheBuilder.newBuilder()
		.maximumSize(5)
		.expireAfterWrite(10, TimeUnit.MINUTES)
		.build()

	private val badNames: Cache<String, Unit> = CacheBuilder.newBuilder()
		.maximumSize(100)
		.expireAfterWrite(1, TimeUnit.HOURS)
		.build()

	@Throws(IOException::class)
	override suspend fun getPokemonInfo(text: String): PokemonInfo? =
		withContext(Dispatchers.IO) {
			Log.i(TAG, "getPokemonInfo: $text")

			run {
				val result = cache.getIfPresent(text)
				if (result != null) return@withContext result
			}

			kotlin.run {
				val result = badNames.getIfPresent(text)
				if (result != null) return@withContext null
			}

			val pokemon = datasource.getPokemon(text) ?: return@withContext kotlin.run {
				badNames.put(text, Unit)
				return@withContext null
			}
			val species = datasource.getSpecies(pokemon.id) ?: return@withContext null

			val result = PokemonInfo(
				pokemon.name,
				pokemon.sprites.front_default,
				pokemon.sprites.front_shiny,
				description = species.flavor_text_entries
					.filter { it.language.name == Locale.getDefault().language }
					.map { entry ->
						entry.copy(
							flavor_text = entry.flavor_text.replace("\n", " ")
						)
					}
			)

			cache.put(text, result)
			result
		}

}