package com.github.doomsdayrs.apps.pokadek.viewmodel

import com.github.doomsdayrs.apps.pokadek.domain.model.PokemonInfo
import kotlinx.coroutines.flow.Flow

interface IMainViewModel {
	val isLoading: Flow<Boolean>
	val content: Flow<PokemonInfo?>
	val exception: Flow<Throwable?>

	fun setText(text: CharSequence)
}